# As an example, this renders onto a pygame surface.
import pygame
from PIL import Image

class RenderInstance(object):

    # We must set here what different windowing systems we natively support.
    # Of-course, we do fall back to a PILLOW image if we don't support it.
    # All windowing systems should support PILLOW images to whatever they natively use.
    supported = ["PYGAME"]

    def __init__(self, w, h):
        """ w -- Integer width
        h -- Integer width
        These values should be the same as your Window."""
        self.surface = pygame.surface.Surface((w,h))

        self.FORCEUSEPIL = False

    def clear(self, colour=(0,0,0)):
        """Clear the internal surface.
        
        colour -- (0-255,0-255,0-255) colour to clear with. By default black."""

        self.surface.fill(colour)

    def stuff(self):
        """Render onto the internal surface."""

        # For debugging purposes, draw a red square
        pygame.draw.rect(self.surface, (255,0,0), pygame.rect.Rect(0,0,32,32))

    def on(self,window):
        """Render ourselves onto the window.
        Will fall-back to PILLOW if the window system isn't supported."""

        if not self.FORCEUSEPIL and window.system in self.supported:
            if window.system == "PYGAME":
                # It's a pygame window, send it the surface.
                window.set_surface(self.surface)
        else:
            # As we don't natively support the renderer, fall back to PIL
            # Convert our internal surface to the PIL image, and send it with the fall back function.
            surfaceRaw = pygame.image.tostring(self.surface, 'RGB')
            surfaceConv = Image.frombytes('RGB', self.surface.get_size(), surfaceRaw)

            window.set_surface_pil(surfaceConv)
            


def init():
    """Initialise any needed libraries or whatever.
    Call it even if it doesn't do anything!"""
    pass
