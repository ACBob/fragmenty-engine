# Handles the logic,
# Pulling together the RENDERER and WINDOWING systems
#--
# HACK Also handles entity logic, but in a proper 'engine', it'd pull together the game logic.
#      But for this test of the concept, we just ignore that as there *is* no logic.
#--

import renderer
from constants import *

def main():

	# You should initialise each 'fragment' in order of importance
	# Logic is the highest, the windowing is next and the renderer is last.
	
	print("Started, Doing logic stuff")
	Run = True

	print("Please choose a windowing system to use.")
	print("1 - PYGAME, 2 - Tkinter")
	c = input(">: ")
	if c == "1":
		import pygamewindow as window
	elif c == "2":
		import tkinterwindow as window
	else:
		print(c,"Is not an option! Quitting.")
		return

	print("Init Window")
	window.init()

	print("Creating a WindowInstance")
	GameWindow = window.WindowInstance(800,600,"")

	print("Init Renderer")
	renderer.init()

	print("Creating a RenderInstance")
	Render = renderer.RenderInstance(800,600)

##	print("Handicapping the RenderInstance for debugging")
##	Render.FORCEUSEPIL = True

	while Run:

		GameWindow.update() # Update the window FIRST

		Render.clear((255,255,0)) # Clear the surface

		Render.stuff() # Render the actual stuff onto the internal surface
		
		# Update the window with the renderer's surface
		Render.on(GameWindow)

		for event in GameWindow.get_events():
			if event.type == window.QUIT:
				print("Quit!")
				Run = False

				break
				

	return 0

if __name__ == "__main__":
	main()
