# For an example, this uses pygame's 'display'.
import pygame
from PIL import Image

# Some constant event values - Make as neccesary.
# However, ALL window systems should have the ones here as standard.

QUIT = pygame.QUIT

# This should generally be how it is set up.
# A WindowInstance handles displaying to the user, and recieving input.
class WindowInstance(object):

    # REQUIRED VALUE FOR ALL OF THEM!
    # We use this when communicating between various areas.
    # Unfortunately, the renderer will RELY on this value.
    system = "PYGAME"

    def __init__(self, w, h, n):
        """ w -- Integer width
        h -- Integer width
        m -- String caption"""
        self.window = pygame.display.set_mode( (w,h) )
        pygame.display.set_caption(n)

    def update(self):
        """Update the windowing system.
        Required for things that happen all the time."""
        pass

    def set_surface(self, otherSurface):
        """Sets the 'surface' of the Window.
        This is how we update what is displayed to the user."""
        self.window.blit(otherSurface, (0,0))

        # Update the window
        pygame.display.flip()

    def set_surface_pil(self, image):
        """Sets the 'surface' of the Window.
        This is how we update what is displayed to the user.
        For compatability, use PIL as a fall-back."""
        
        imageRaw = image.tobytes("raw", 'RGB')
        otherSurface = pygame.image.fromstring(imageRaw, image.size, 'RGB')
        
        self.window.blit(otherSurface, (0,0))

        # Update the window
        pygame.display.flip()

    def get_events(self):
        """Returns the event queue.
        Destroys the event queue."""
        return pygame.event.get()
    
    def get_event(self):
        """Returns a SINGLE item from the event queue.
        This destroys the item in the queue."""
        return pygame.event.poll()


def init():
    """Initialise any needed libraries or whatever.
    Call it even if it doesn't do anything!"""
    pygame.display.init()


if __name__ == "__main__":
    print("Testing Windowing system")
    a = WindowInstance(800,600,"WINDOWING TEST")
    print("Created WindowInstance")

    while True:
        if a.get_event().type == pygame.QUIT:
            print("Recieved QUIT")
            break

