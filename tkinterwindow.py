# This one is pretty much a copy of pygame's one, but with pygame ripped out and replaced with tkinter.
# Comments remain from the pygame version.
# Bit difficult as tkinter is a gui library rather than a window one, but here's hopin'.
# I'll also need to circumvent the event loop system, i imagine.
import tkinter
from PIL import Image, ImageTk

# Some constant event values - Make as neccesary.
# However, ALL window systems should have the ones here as standard.

QUIT = 2^1 # Code for the QUIT event.

# This should generally be how it is set up.
# A WindowInstance handles displaying to the user, and recieving input.
class WindowInstance(object):

    # REQUIRED VALUE FOR ALL OF THEM!
    # We use this when communicating between various areas.
    # Unfortunately, the renderer will RELY on this value.
    system = "TKINTER"

    def __init__(self, w, h, n):
        """ w -- Integer width
        h -- Integer width
        m -- String caption"""
        self.window = tkinter.Tk()
        self.window.title(n)
        self.window.geometry("{x}x{y}".format(x=w,y=h))

        # Disable resizing by default
        self.window.resizable(False,False)

        # We also add a canvas to the window, as the 'screen'
        self.canvas = tkinter.Canvas(self.window, width=w, height=h)
        self.canvas.pack()

        self.event_queue = []

        self.window.protocol('WM_DELETE_WINDOW',self._quitevent)

        self.image = None # we need to keep a reference to the image, so we keep it in ourselves
        self.image_id = None # Tkinter allows us to change the image with an ID

    def update(self):
        """Update the windowing system.
        Required for things that happen all the time."""
        self.window.update()

    def set_surface(self, otherSurface):
        """Sets the 'surface' of the Window.
        This is how we update what is displayed to the user."""
        raise NotImplementedError("This windowing system doesn't have a native system.")

    def set_surface_pil(self, image):
        """Sets the 'surface' of the Window.
        This is how we update what is displayed to the user.
        For compatability, use PIL as a fall-back."""

        # Keep a reference to the image, and also convert it
        self.image = ImageTk.PhotoImage(image)

        # Apply it to the canvas
        if not self.image_id:
            self.canvas.create_image((0,0),image=self.image,anchor="nw")
        else:
            self.canvas.itemconfig(self.image_id, image=self.image)

        # Update ourselves with the new image
        self.update()

    def get_events(self):
        """Returns the event queue.
        Destroys the event queue."""
        events = self.event_queue.copy()
        self.event_queue = []
        return events
    
    def get_event(self):
        """Returns a SINGLE item from the event queue.
        This destroys the item in the queue."""
        return self.event_queue.pop()

    def _event(self,g):
        """Not to be called outside of the window system.
        Internally used to catch events from the tkinter window."""
        raise NotImplementedError("Implement for events that need captured")

    def _quitevent(self):
        """Internally used to add a quit event to the queue."""
        self.event_queue.append(WindowEvent(QUIT))

class WindowEvent(object):
	"""A Small wrapper around window events"""

	def __init__(self, type):
		self.type = type

def init():
    """Initialise any needed libraries or whatever.
    Call it even if it doesn't do anything!"""
    pass


if __name__ == "__main__":
    print("Testing Windowing system")
    a = WindowInstance(800,600,"WINDOWING TEST")
    print("Created WindowInstance")

    while True:
        pass

