Well, i call it an *engine* but it's actually just a quick implementation of an idea i had.

This idea came to be at the prime time of 1:56, where i proceeded to get out of bed (after only being in it for a few minutes!) and just bash out the idea. It seems promising, so i might continue it and slowly make a game engine out of it. No idea, and sleep deprevation is calling so i'll cut this readme short. (I say as i go on to write a full essay on this design)

---

the basic design philosophy is that each seperate part of the engine would be 'fragmented' into different sections. meaning that the following things would ideally be seperate
 * Logic
 * Rendering
 * Window System
 * Sound
 * I/O
 * 'The Mixer' - The magic that combines all of the previous into one, *hopefully* working system.

And these seperate parts are called 'Fragments'. The logic would be the actual game or whatever this is for, and it would call functions on the rendering, window, sound, etc. and you could swap these out.

For example, you could replace the renderer quite easily, without touching any game code. As long as you define the correct variables, and have the right functions, it would mesh seamlessly and the rest of the system just *wouldn't care*.

At present, this repo contains two window system fragments, one for Pygame and the other for Tkinter. When you run main, it asks you which one you want, and you can pretty easily see the fact that the logic doesn't care, beyond choosing which to import.

Obviously, there's not much reason to render with pygame and then use a tkinter gui, but it's a proof of concept. What might be a better test is using OpenGL to render, and then you can choose whether to use a tkinter window or a pygame window.

I've not actually tested performence, but i'd imagine the tkinter one is significantly slower than just using pygame. There's an awful lot of conversion between image formats involved in setting the screen.

---

Right now there's nothing concrete, but i'm imagining that if i were to continue this, there'd be 'Feature-sets' of different abilities. For instance, there could be a standard rendering feature set, that has displaying and basic stuff. There could also then be an extended feature set, maybe with post processing that the game logic could call, or whatever. They'd declare their feature set (Much like how in the current implementation they declare their supported versions) and shout at each other.

---

This python version is just a fast mockup, so the real thing would probably be C++, with each 'fragment' being a DLL. Although i'd probably design it in a way that you could compile it as one - defeating the purpose, but making it useful for people that want to use it, but don't want users to use the fragmenting abilities.

Although it'd be interesting to see a minecraft clone or smth that exposes its renderer, and then someone uses that on, say, a platformer. It'd be exactly what this design hopes to allow.

---

I don't know, it's nearly 4 in the morning and i'm tired lole

There's probably better designs/systems already out there in production, i just imagined this and wanted *something* like it. It really wouldn't surprise me if there's a system exactly like this already out there and in wide-spread use.